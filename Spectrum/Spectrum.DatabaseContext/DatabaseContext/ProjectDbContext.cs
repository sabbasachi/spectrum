﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Spectrum.Model.Model;

namespace Spectrum.DatabaseContext.DatabaseContext
{
    public class ProjectDbContext: DbContext
    {
        public ProjectDbContext()
        {
            Configuration.LazyLoadingEnabled = false;
        }

        public  DbSet<Customer> Customers { set; get; }
        public DbSet<Supplier> Supplier { set; get; }
        public DbSet<Category> Category { set; get; }

    }
}
