﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Spectrum.Model.Model;
using Spectrum.DatabaseContext.DatabaseContext;

namespace Spectrum.Repository.Repository
{
    public class CategoryRepository
    {
        ProjectDbContext _dbContext = new ProjectDbContext();
        public bool Add(Category category)
        {
            _dbContext.Category.Add(category);
            return _dbContext.SaveChanges() > 0;
        }

        public List<Category> GetAll()
        {
            return _dbContext.Category.ToList();
        }

        public Category getById(int id)
        {
            return _dbContext.Category.FirstOrDefault(c => c.Id == id);
        }
        public bool Update(Category category)
        {
            Category aCategory = _dbContext.Category.FirstOrDefault(c => c.Id == category.Id);
            if (aCategory != null)
            {
                aCategory.Code = category.Code;
                aCategory.Name = category.Name;
            }
            return _dbContext.SaveChanges() > 0;
        }
        public bool Delete(int id)
        {
            Category aCategory = _dbContext.Category.FirstOrDefault(c => c.Id == id);
            _dbContext.Category.Remove(aCategory);
            return _dbContext.SaveChanges() > 0;
        }
    }
}
