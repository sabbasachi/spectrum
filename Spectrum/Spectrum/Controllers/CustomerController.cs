﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Spectrum.Model.Model;
using Spectrum.Bll.Bll;
using Spectrum.Models;
using AutoMapper;

namespace Spectrum.Controllers
{
    public class CustomerController : Controller
    {
        CustomerManager _customerManager = new CustomerManager();
        [HttpGet]
        public ActionResult Add()
        {
            CustomerViewModel customerViewModel = new CustomerViewModel();
            customerViewModel.Customers = _customerManager.GetAll();
            return View(customerViewModel);
            
        }


        [HttpPost]
        public ActionResult Add(CustomerViewModel customerViewModel)
        {
            string message = "";
            if (ModelState.IsValid)
            {
                Customer customer = Mapper.Map<Customer>(customerViewModel);
                if (_customerManager.Add(customer))
                {
                    message = "Saved";
                }
                else
                {
                    message = "Not Saved";
                }
            }
            else
            {
                message = "ModelState Failed";
            }

            ViewBag.Message = message;
            customerViewModel.Customers = _customerManager.GetAll();
            return View(customerViewModel);
        }

        [HttpGet]
        public ActionResult Search()
        {
            CustomerViewModel customerViewModel = new CustomerViewModel();
            customerViewModel.Customers = _customerManager.GetAll();
            return View(customerViewModel);
        }

        [HttpPost]
        public ActionResult Search(CustomerViewModel customerViewModel)
        {
            var customers = _customerManager.GetAll();
            if (customerViewModel.Code != null)
            {
                customers = customers.Where(c => c.Code.Contains(customerViewModel.Code)).ToList();
            }
            if (customerViewModel.Name != null)
            {
                customers = customers.Where(c => c.Name.Contains(customerViewModel.Name)).ToList();
            }
            if (customerViewModel.Contact != null)
            {
                customers = customers.Where(c => c.Contact.Contains(customerViewModel.Contact)).ToList();
            }
            if (customerViewModel.Email != null)
            {
                customers = customers.Where(c => c.Email.Contains(customerViewModel.Email)).ToList();
            }

            customerViewModel.Customers = customers;

            return View(customerViewModel);
        }
        [HttpGet]
        public ActionResult Edit(int id)
        {
            var customer = _customerManager.getById(id);
            CustomerViewModel customerViewModel = Mapper.Map<CustomerViewModel>(customer);
            customerViewModel.Customers = _customerManager.GetAll();

            return View(customerViewModel);

        }
        [HttpPost]
        public ActionResult Edit(CustomerViewModel customerViewModel)
        {
            string message = "";
            if (ModelState.IsValid)
            {
                Customer customer = Mapper.Map<Customer>(customerViewModel);
                if (_customerManager.Update(customer))
                {
                    message = "Updated!";
                }
                else
                {
                    message = "Not Updated";
                }
            }
            else
            {
                message = "ModelState Failed";
            }

            ViewBag.Message = message;
            customerViewModel.Customers = _customerManager.GetAll();
            return View(customerViewModel);
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            var customer = _customerManager.getById(id);
            CustomerViewModel customerViewModel = Mapper.Map<CustomerViewModel>(customer);
            customerViewModel.Customers = _customerManager.GetAll();

            return View(customerViewModel);

        }
        [HttpPost]
        public ActionResult Delete(CustomerViewModel customerViewModel)
        {
            string message = "";
            if (ModelState.IsValid)
            {
                Customer customer = Mapper.Map<Customer>(customerViewModel);
                if (_customerManager.Delete(customer.Id))
                {
                    message = "Deleted!!";
                }
                else
                {
                    message = "Not Deleted";
                }
            }
            else
            {
                message = "ModelState Failed";
            }

            ViewBag.Message = message;
            customerViewModel.Customers = _customerManager.GetAll();
            return View(customerViewModel);
        }
    }
}